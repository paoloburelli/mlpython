# mlpython #

Docker container based on [python 3.5 official container](https://github.com/docker-library/python/tree/master/3.5) including a number of machine learning and data processing libraries.
Designed to speedup deployment to docker for sklearn based projects.

The images can be found at: https://hub.docker.com/r/paoloburelli/mlpython/