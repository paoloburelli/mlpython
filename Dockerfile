FROM python:3.5

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements_default.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements_default.txt
